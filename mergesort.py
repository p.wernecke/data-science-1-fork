def ASSIGNMENT(new_list, i, old_list, j):
    new_list[i] = old_list[j]


def mergeSort(list_to_sort_by_merge):
    if (
        len(list_to_sort_by_merge) > 1
        and not len(list_to_sort_by_merge) < 1
        and len(list_to_sort_by_merge) != 0
    ):
        mid = len(list_to_sort_by_merge) // 2
        left = list_to_sort_by_merge[:mid]
        right = list_to_sort_by_merge[mid:]

        mergeSort(left)
        mergeSort(right)

        l = 0
        r = 0
        i = 0

        while l < len(left) and r < len(right):
            if left[l] <= right[r]:
                ASSIGNMENT(new_list=list_to_sort_by_merge, i=i, old_list=left, j=l)
                l += 1
            else:
                ASSIGNMENT(new_list=list_to_sort_by_merge, i=i, old_list=right, j=r)
                r += 1
            i += 1

        while l < len(left):
            list_to_sort_by_merge[i] = left[l]
            l += 1
            i += 1

        while r < len(right):
            list_to_sort_by_merge[i] = right[r]
            r += 1
            i += 1


# --- THE FOLLOWING PART WAS OPTIMIZED BY CHATGPT ---
import matplotlib.pyplot as plt

def plot_list(data, title, line_style='-', marker='o', color='black'):
    x = range(len(data))
    plt.plot(x, data, linestyle=line_style, marker=marker, color=color, label=title)
    plt.title(title)
    plt.xlabel('Index')
    plt.ylabel('Value')
    plt.grid(True)
    plt.legend()
    plt.show()

my_list = [54, 26, 93, 17, 77, 31, 44, 55, 20]

# Plot the list before sorting with high contrast color and distinct marker
plot_list(my_list, "Before Sorting", line_style='-', marker='o', color='blue')

# Sort the list
mergeSort(my_list)

# Plot the list after sorting with high contrast color and distinct marker
plot_list(my_list, "After Sorting", line_style='--', marker='x', color='orange')